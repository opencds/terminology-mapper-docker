# Version: 0.0.1

FROM ubuntu:14.04
MAINTAINER Salvador Rodriguez <salvador.rodriguez@utah.edu>

# Install packages
ENV REFRESHED_AT 2015-03-10
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && \
    apt-get install -yq --no-install-recommends wget unzip pwgen ca-certificates && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

RUN \
  apt-get update && \
  apt-get install -y openjdk-7-jre && \
  rm -rf /var/lib/apt/lists/*

ENV JAVA_HOME /usr/lib/jvm/java-7-openjdk-amd64
RUN echo PATH=$JAVA_HOME/bin:$PATH:$HOME/bin >> ~/.bashrc
RUN echo export PATH JAVA_HOME >> ~/.bashrc

# Install Tomcat 7
RUN wget http://archive.apache.org/dist/tomcat/tomcat-7/v7.0.57/bin/apache-tomcat-7.0.57.zip
RUN unzip apache-tomcat-7.0.57.zip
RUN rm -f apache-tomcat-7.0.57.zip
RUN mv /apache-tomcat-7.0.57 /tomcat
ADD tomcat-users.xml /tomcat/conf/tomcat-users.xml
RUN cd /tomcat/bin/; chmod +x *.sh

# Install Terminology mapper
ADD mapper-config.groovy /root/.opencds/mapper-config.groovy
ADD mapper-config.properties /root/.opencds/mapper-config.properties
ADD mapper.war /tomcat/webapps/mapper.war

EXPOSE 8080
CMD ["sh", "/tomcat/bin/catalina.sh", "run"]

