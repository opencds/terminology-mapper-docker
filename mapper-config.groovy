// environment specific settings
environments {
	production {
		dataSource {
			driverClassName = "com.mysql.jdbc.Driver" 
            		url = "jdbc:mysql://db:3306/umls?useUnicode=yes&characterEncoding=UTF-8"
			username = "umls_user"
			password = "umls"			
		}
	}
}
