You must include the mapper.war file in the same folder where Dockerfile is located and remove the comments from Dockerfile on ADD mapper.war /tomcat/webapps/mapper.war  

Build image :
sudo docker build -t="opencds/mapper" .

Create container and with a link to umlsdb and a link to opencds-apelon container (for example) :
sudo docker run -i -t --name mapper --link umlsdb:db --link opencds_apelon:oa opencds/mapper

or daemon container:
sudo docker run -d --name mapper --link umlsdb:db --link opencds_apelon:oa opencds/mapper

Terminology mapper will be available on x.x.x.x:8080/mapper.

Find out the ipaddress of the container:
sudo docker inspect mapper | grep IPAddress
