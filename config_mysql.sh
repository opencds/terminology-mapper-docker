#!/bin/bash

__mysql_config() {
echo "Running the mysql_config function."
chown -R mysql:mysql /var/lib/mysql
/etc/init.d/mysql start
update-rc.d mysql defaults
sleep 10
}

__start_mysql() {
echo "Running the start_mysql function."
mysqladmin -u root password mysqlPassword
mysql -uroot -pmysqlPassword -e "CREATE DATABASE umls"
mysql -uroot -pmysqlPassword -e "CREATE USER 'umls_user'@'localhost' IDENTIFIED BY 'umls';"
mysql -uroot -pmysqlPassword -e "GRANT ALL PRIVILEGES ON *.* TO 'umls_user'@'localhost' WITH GRANT OPTION;"		 
killall mysqld
sleep 10
}

# Call all functions
__mysql_config
__start_mysql
